var element1 = document.getElementById('controledClass1');
var element2 = document.getElementById('controledClass2');
var element3 = document.getElementById('controledClass3');
var checkValue = document.querySelectorAll('input');
var postUrl = 'https://sandbox.sbm24.net/api/v1/site/doctor/post-star-rate';
var postCommentUrl = 'https://sandbox.sbm24.net/api/v1/site/doctor/post-star-rate';

const user_id = 1;
const doctor_id = 2;

function ratingStar(event) {
	for (var i = 0; i < checkValue.length; i++) {
		if (checkValue[i] == event.target) {
			let data = new FormData();
			switch (i + 1) {
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
					element1.classList.add('star-rating__starsInActive');
					data.append('user_id', user_id);
					data.append('doctor_id', doctor_id);
					data.append('quality', i + 1);

					console.warn(checkValue);

					fetch(postUrl, {
						method: 'post',
						body: data
					}).then(function(res) {
						return res;
					});
					break;
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
					element2.classList.add('star-rating__starsInActive');
					data.append('user_id', user_id);
					data.append('doctor_id', doctor_id);
					data.append('cost', i + 1 - 5);

					console.warn(checkValue);

					fetch(postUrl, {
						method: 'post',
						body: data
					}).then(function(res) {
						return res;
					});
					break;
				case 11:
				case 12:
				case 13:
				case 14:
				case 15:
					element3.classList.add('star-rating__starsInActive');
					data.append('user_id', user_id);
					data.append('doctor_id', doctor_id);
					data.append('behaviour', i + 1 - 10);

					console.warn(checkValue);

					fetch(postUrl, {
						method: 'post',
						body: data
					}).then(function(res) {
						return res;
					});
					break;
				default:
					console.log(i);
			}
		}
	}
}

function sendComment(event) {
	event.preventDefault();
	let data = new FormData();
	var postCommentUrl = 'https://sandbox.sbm24.net/api/v1/site/doctor/post-star-rate';
	var comment = document.getElementById('myTextarea').value;
	data.append('user_id', user_id);
	data.append('doctor_id', doctor_id);
	data.append('comment', comment);

	fetch(postCommentUrl, {
		method: 'post',
		body: data
	}).then(function(res) {
		return res;
	});
}
